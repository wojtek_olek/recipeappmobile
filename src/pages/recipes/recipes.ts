import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EditRecipePage } from '../edit-recipe/edit-recipe';
import { Recipe } from '../../models/recipe.model';
import { RecipesService } from '../../services/recipes.service';
import { RecipePage } from '../recipe/recipe';
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {
  editPage = EditRecipePage;
  recipes: Recipe[] = [];

  constructor(private navCtrl: NavController, 
              private navParams: NavParams,
              private recipeService: RecipesService,
              private authService: AuthService) {
  }

  ionViewWillEnter(){
    this.recipes = this.recipeService.getRecipes();
  }

  ionViewCanEnter(){
    if (this.recipes.length == 0) {
      this.fetchRecipes();
    }
  }

  loadRecipe(index: number) {
    this.navCtrl.push(RecipePage, {index: index});
  }

  fetchRecipes() {
    this.authService
      .getActiveUser()
      .getIdToken()
      .then((token: string) => {
        this.recipeService.fetchRecipes(token).subscribe(
          (list: Recipe[]) => {
            if (list) {
              this.recipes = list;
            } else {
              this.recipes = [];
            }
          },
          error => {
            console.log(error);
          }
        );
      });
  }
}
