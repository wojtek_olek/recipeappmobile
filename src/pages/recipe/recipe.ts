import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Recipe } from '../../models/recipe.model';
import { EditRecipePage } from '../edit-recipe/edit-recipe';
import { RecipesService } from '../../services/recipes.service';
import { ShoppingListService } from '../../services/shopping-list.service';

@IonicPage()
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html',
})
export class RecipePage implements OnInit {
  recipeIndex: number;
  recipe: Recipe;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private recipesService: RecipesService,
              private shoppingListService: ShoppingListService,
              private toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.recipeIndex = this.navParams.get("index");
    this.recipe = this.recipesService.getRecipe(this.recipeIndex);
  }

  ionViewWillEnter(){
    this.recipesService.recipeChanged.subscribe((recipes:Recipe[]) => {
      this.recipe = recipes[this.recipeIndex];
    })
  }

  addIngredientsToSL() {
    const ingList = this.recipe.ingredients;
    this.shoppingListService.addIngredients(ingList);
    const toSLToast = this.toastCtrl.create({
      message: "Ingredients added to Shopping List :)",
      duration: 2000
    });
    toSLToast.present();
  }

  editRecipe() {
    this.navCtrl.push(EditRecipePage, {mode: 'Edit', recipe: this.recipe, index: this.recipeIndex })
  }

  deleateRecipe() {
    const deleateToast = this.toastCtrl.create({
      message: "Recipe deleated :(",
      duration: 2000
    });
    this.recipesService.removeRecipe(this.recipeIndex);
    deleateToast.present();
    this.navCtrl.popToRoot();
  }

}
