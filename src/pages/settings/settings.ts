import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthService) {
  }

  onLogout() {
    this.authService.logout();
    this.navCtrl.popToRoot();
  }

}
