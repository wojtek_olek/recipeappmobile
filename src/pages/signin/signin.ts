import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SignupPage } from '../signup/signup';
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: "page-signin",
  templateUrl: "signin.html"
})
export class SigninPage {
  signinForm: FormGroup;
  signupPage = SignupPage;
  error = false;
  errorMessage = '';

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private loadingCtrl: LoadingController
  ) {
    this.createForm();
  }

  createForm() {
    this.signinForm = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  onSubmit() {
    this.error = false;
    let email = this.signinForm.get("email").value;
    let password = this.signinForm.get("password").value;

    const loading = this.loadingCtrl.create({
      content: 'Signing you in...'
    })
    loading.present();
    this.authService.signin(email, password).then((data) => {
      loading.dismiss()
    }).catch((error) => {
      this.errorMessage = error.message;
      this.error = true;
      loading.dismiss()
    });
  }
}
