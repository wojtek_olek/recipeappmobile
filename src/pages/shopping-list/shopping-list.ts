import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ShoppingListService } from "../../services/shopping-list.service";
import { Ingredient } from "../../models/ingredient.model";
import { SettingsPage } from "../settings/settings";
import { AuthService } from "../../services/auth.service";

@IonicPage()
@Component({
  selector: "page-shopping-list",
  templateUrl: "shopping-list.html"
})
export class ShoppingListPage {
  settingsPage = SettingsPage;
  ingredients: Ingredient[];
  newIngredientForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private shoppinglistService: ShoppingListService,
    private authService: AuthService
  ) {
    this.createForm();
  }

  ionViewWillEnter() {
    this.shoppinglistService.slChanged.subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      }
    );
  }

  ionViewCanEnter() {
    this.fetchIngredients();
  }

  private createForm() {
    this.newIngredientForm = this.formBuilder.group({
      name: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(30)
        ])
      ],
      amount: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(5)
        ])
      ]
    });
  }

  addNewIngredient() {
    let ingredient = {
      name: this.newIngredientForm.get("name").value,
      amount: this.newIngredientForm.get("amount").value
    };
    this.shoppinglistService.addIngredient(ingredient);
    this.createForm();
    this.storeIngredients();
  }

  onRemoveIngredient(index: number) {
    this.shoppinglistService.removeIngredient(index);
    this.storeIngredients();
  }

  storeIngredients() {
    this.authService
      .getActiveUser()
      .getIdToken()
      .then((token: string) => {
        this.shoppinglistService.storeList(token).subscribe(
          () => {
            console.log("Success");
          },
          error => {
            console.log(error);
          }
        );
      });
  }

  fetchIngredients() {
    this.authService
      .getActiveUser()
      .getIdToken()
      .then((token: string) => {
        this.shoppinglistService.fetchList(token).subscribe(
          (list: Ingredient[]) => {
            if (list) {
              this.ingredients = list;
            } else {
              this.ingredients = [];
            }
          },
          error => {
            console.log(error);
          }
        );
      });
  }
}
