import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavParams,
  ActionSheetController,
  AlertController,
  ToastController,
  NavController
} from "ionic-angular";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from "@angular/forms";
import { RecipesService } from "../../services/recipes.service";
import { Recipe } from "../../models/recipe.model";
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: "page-edit-recipe",
  templateUrl: "edit-recipe.html"
})
export class EditRecipePage implements OnInit {
  mode = "New";
  diffOptions = ["Easy", "Medium", "Hard"];
  recipeForm: FormGroup;
  recipe: Recipe;
  index: number;

  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private toastControler: ToastController,
    private recipeService: RecipesService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.mode = this.navParams.get("mode");

    if (this.mode == "Edit") {
      this.recipe = this.navParams.get("recipe");
      this.index = this.navParams.get("index");
    }

    this.createForm();
  }

  private createIngredient(nameI, amountI) {
    return new FormGroup({
      name: new FormControl(nameI, Validators.required),
      amount: new FormControl(amountI, Validators.required)
    });
  }

  private createForm() {
    let title = null;
    let description = null;
    let difficulty = "Medium";
    let ingredients = [];

    if (this.mode == "Edit") {
      title = this.recipe.title;
      description = this.recipe.description;
      difficulty = this.recipe.difficulty;
      for (let ingredient of this.recipe.ingredients) {
        ingredients.push(
          this.createIngredient(ingredient.name, ingredient.amount)
        );
      }
    }

    this.recipeForm = this.formBuilder.group({
      title: [
        title,
        Validators.compose([
          Validators.required,
          Validators.maxLength(30),
          Validators.minLength(4)
        ])
      ],
      description: [
        description,
        Validators.compose([
          Validators.required,
          Validators.maxLength(120),
          Validators.minLength(1)
        ])
      ],
      difficulty: [difficulty, Validators.required],
      ingredients: this.formBuilder.array(ingredients)
    });
  }

  onSubmit() {
    let recipe = {
      title: this.recipeForm.get("title").value,
      description: this.recipeForm.get("description").value,
      difficulty: this.recipeForm.get("difficulty").value,
      ingredients: this.recipeForm.get("ingredients").value
    };
    let submitToast;

    if (this.mode == "Edit") {
      submitToast = this.toastControler.create({
        message: "Recipe updated!",
        duration: 2000
      });
      this.recipeService.updateRecipe(this.index, recipe);
    } else {
      submitToast = this.toastControler.create({
        message: "Recipe added!",
        duration: 2000
      });
      this.recipeService.addRecipe(recipe);
    }
    this.storeRecipes();
    submitToast.present();
    this.navCtrl.pop();
  }

  manageIngredients() {
    console.log(this.recipeForm);
    const actionSheet = this.actionSheetController.create({
      title: "What do you want to do?",
      buttons: [
        {
          text: "Add ingredient",
          handler: () => {
            this.createNewIngredientAlert().present();
          }
        },
        {
          text: "Remove all ingredients",
          role: "destructive",
          handler: () => {
            const ingArray = <FormArray>this.recipeForm.get("ingredients");
            const ingArrayLength = ingArray.length;
            if (ingArrayLength > 0) {
              for (let i = ingArrayLength - 1; i >= 0; i--) {
                ingArray.removeAt(i);
              }
            }
          }
        },
        { text: "Cancel", role: "cancel" }
      ]
    });
    actionSheet.present();
  }

  private createNewIngredientAlert() {
    const newIngAlert = this.alertController.create({
      title: "Add Ingredient",
      inputs: [
        { name: "name", placeholder: "Name" },
        { name: "amount", placeholder: "Amount" }
      ],
      buttons: [
        { text: "Cancel", role: "cancel" },
        {
          text: "Add",
          handler: data => {
            if (data.name.trim() == "" || data.name == null) {
              const toast = this.toastControler.create({
                message: "Please enter a valid value!",
                duration: 2000
              });
              toast.present();
              return;
            }
            (<FormArray>this.recipeForm.get("ingredients")).push(
              this.createIngredient(data.name, parseInt(data.amount, 10))
            );
          }
        }
      ]
    });
    return newIngAlert;
  }

  storeRecipes() {
    this.authService
      .getActiveUser()
      .getIdToken()
      .then((token: string) => {
        this.recipeService.storeRecipes(token).subscribe(
          () => {
            console.log("Success");
          },
          error => {
            console.log(error);
          }
        );
      });
  }
}
