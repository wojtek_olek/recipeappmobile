import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  signupForm: FormGroup;
  error = false;
  errorMessage = '';

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private loadingCtrl: LoadingController ,
    private toastCtrl: ToastController
  ) {
    this.createForm();
  }

  createForm() {
    this.signupForm = this.formBuilder.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(5),
          Validators.maxLength(30)
        ])
      ],
      password: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30)
        ])
      ]
    });
  }

  onSubmit() {
    this.error = false;
    let email = this.signupForm.get("email").value;
    let password = this.signupForm.get("password").value;

    const loading = this.loadingCtrl.create({
      content: 'Signing you up...'
    });
    const toast = this.toastCtrl.create({
      message: "Successfuly create account and loged in!",
      duration: 3000
    });
    loading.present();
    this.authService.signup(email, password).then((data) => {
      loading.dismiss();
      toast.present();
      this.navCtrl.pop();
    }).catch((error) => {
      loading.dismiss();
      this.errorMessage = error.message;
      this.error = true;
    });
  }
}
