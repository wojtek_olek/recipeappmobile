import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Ingredient } from "../models/ingredient.model";
import { Subject } from "rxjs/Subject";
import { AuthService } from "./auth.service";
import "rxjs/Rx";

@Injectable()
export class ShoppingListService {
  slChanged = new Subject<Ingredient[]>();

  private ingredientsList: Ingredient[] = [];

  constructor(private http: Http, private authService: AuthService) {}

  getIngredients() {
    return this.ingredientsList.slice();
  }

  getIngredient(index: number) {
    return this.ingredientsList[index];
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredientsList.push(ingredient);
    this.slChanged.next(this.ingredientsList.slice());
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingredientsList.push(...ingredients);
    this.slChanged.next(this.ingredientsList.slice());
  }

  removeIngredient(index: number) {
    this.ingredientsList.splice(index, 1);
    this.slChanged.next(this.ingredientsList.slice());
  }

  storeList(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http
      .put(
        "https://recipebookmobile-71ba4.firebaseio.com/" +
          userId +
          "/shopping-list.json?auth=" +
          token,
        this.ingredientsList
      )
      .map((response: Response) => {
        return response.json();
      });
  }

  fetchList(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http
      .get(
        "https://recipebookmobile-71ba4.firebaseio.com/" +
          userId +
          "/shopping-list.json?auth=" +
          token
      )
      .map((response: Response) => {
        let ingredients: Ingredient[] = response.json() ? response.json() : [];
        if (!ingredients) {
          ingredients = [];
        }
        return ingredients;
      })
      .do(data => {
        this.ingredientsList = data;
      });
  }
}
