import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { Subject } from 'rxjs/Subject';
import { Http, Response } from '@angular/http';
import { AuthService } from './auth.service';

@Injectable()
export class RecipesService {

  private recipes: Recipe[] = [];
  recipeChanged = new Subject<Recipe[]>();

  constructor(private http: Http, private authService: AuthService) {}

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  updateRecipe(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  removeRecipe(index: number) {
    this.recipes.splice(index, 1);
  }
  
  storeRecipes(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http
      .put(
        "https://recipebookmobile-71ba4.firebaseio.com/" +
          userId +
          "/recipes-list.json?auth=" + token,
        this.recipes
      )
      .map((response: Response) => {
        return response.json();
      });
  }

  fetchRecipes(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http.get(
      "https://recipebookmobile-71ba4.firebaseio.com/" +
        userId +
        "/recipes-list.json?auth=" + token).map((response: Response) => {
          const recipes: Recipe[] = response.json() ? response.json() : [];
          for (let recipe of recipes) {
            if (!recipe["ingredients"]) {
              recipe["ingredients"] = [];
            }
          }
          return recipes;
        }).do((data) => {
          this.recipes = data; 
        });
  }
}